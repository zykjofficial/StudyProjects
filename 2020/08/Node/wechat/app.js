const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = 3000
// 用于存放用户信息
const users = []

server.listen(port, () => {
    console.log("服务启动成功、监听端口" + port);
});

// express处理静态资源
// 把public目录没置为静态资源
app.use(require("express").static('public'))


app.get("/", (req, res) => {
    res.redirct("/index.html")
})

io.on("connection", socket => {
    // console.log("用户连接成功！");
    socket.on("login", data => {
        //判断，如果data在users中存在，说明该用户已经登录了，不允许登录
        //如果data在users中不存在，说明该用户没有登采，允许用户登求
        let user = users.find(item => item.username === data.username)
        if (user) {
            // 用户存在 登录失败
            socket.emit("loginError", { msg: "登录失败、用户名已存在！" })
            // console.log("登录失败!");
        } else {
            console.log(`用户 ${data.username} 连接成功！`);
            // 用户不存在 登录成功    
            users.push(data)
            socket.emit("loginSuccess", data)
            // console.log("登录成功！");

            // 告诉所有的用户，有用户加入到了聊天室，广播消息
            // socket.emit: 告诉当前用户
            // io.emit: 广播事件
            io.emit("addUser", data)

            // 告诉告诉所有的用户，目前聊天室中有多少人
            io.emit('userList', users)

            //登录成功后存储用户名和头像
            socket.username = data.username
            socket.avatar = data.avatar
        }
    })


    //用户断升连接的功能
    //监听用户断开连接
    socket.on('disconnect', () => {
        if (socket.username != undefined) {
            // 把当前用户的信息从users中删除掉
            let idx = users.findIndex(item => item.username === socket.username)
            users.splice(idx, 1)
            // 1．告诉所有人，有人离开了聊天室
            io.emit("delUser", {
                username: socket.username,
                avatar: socket.avatar
            })

            // 2. 告诉所有人 userList 发生更新
            io.emit("userList", users)
        }

    })

    // 发送给服务器
    socket.on("sendMessage", data => {
        // 广播
        io.emit("receiveMessage", data)
    })

    // 发送给服务器
    socket.on("sendImage", data => {
        // 广播
        io.emit("receiveImage", data)
    })
})

