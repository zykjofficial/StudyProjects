/* 聊天室的主要功能 */

// 1.连接socketio服务
var socket = io('http://localhost:3000/')
var username, avatar

/**
 * 登录功能
 */

$('#login_avatar li').on("click", function () {
  $(this).addClass("now").siblings().removeClass("now")
})


// 输入用户名
$('#loginBtn').on('click', function () {
  username = $('#username').val().trim()
  if (!username) {
    alert("请输入用户名")
    return
  }
  avatar = $('#login_avatar li.now img').attr('src')
  socket.emit("login", {
    username: username,
    avatar: avatar
  })
})


/**
 * 登录状态返回
 */

socket.on("loginError", data => {
  alert(data.msg)
})

socket.on("loginSuccess", data => {
  // 登录成功
  // 隐藏登录窗口
  // $('.login_box').fadeOut()
  $('.login_box').hide()
  // 显示聊天窗口
  $('.container').fadeIn()
  // 设置个人信息
  $('.user-list .avatar_url').attr('src', data.avatar)
  $('.user-list .username').text(data.username)
})


// 用户进入群聊
socket.on("addUser", data => {
  $('.box-bd').append(`
  <div class="system">
  <p class="message_system">
    <span class="content">${data.username} 加入了群聊</span>
  </p>
</div>`)
})

// 用户退出群聊
socket.on("delUser", data => {
  $('.box-bd').append(`
  <div class="system">
  <p class="message_system">
    <span class="content">${data.username} 退出了群聊</span>
  </p>
</div>`)
})

// 用户数量
socket.on("userList", data => {
  $('.user-list ul').html('')
  //将用户添加到左侧菜单中
  data.forEach(item => {
    $('.user-list ul').append(`
    <li class="user">
          <div class="avatar"><img src="${item.avatar}" alt=""></div>
          <div class="name">${item.username}</div>
    </li>
    `)
  });

  $('#userCount').text(data.length)
})

// 发送消息
$('#btn-send').on("click", function () {
  // 获取文本内容
  var content = $('#content').html().trim()
  $('#content').html('')
  if (!content) return alert("请输入内容")
  socket.emit("sendMessage", {
    msg: content,
    username: username,
    avatar: avatar
  })

})

// 监听聊天消息
socket.on("receiveMessage", data => {
  // 把接收到的消息显示到聊天框中
  if (data.username === username) {
    // 自己的消息
    $('.box-bd').append(`
    <div class="message-box">
      <div class="my message">
        <img src="${data.avatar}" alt="" class="avatar">
        <div class="content">
        <div class="bubble">
          <div class="bubble_cont">${data.msg}</div>
        </div>
      </div>
      </div>
  </div>
    `)
  } else {
    // 别人的消息
    $('.box-bd').append(`
    <div class="message-box">
      <div class="other message">
        <img src="${data.avatar}" alt="" class="avatar">
        <div class="nickname">${data.username}</div>
        <div class="content">
          <div class="bubble">
            <div class="bubble_cont">${data.msg}</div>
          </div>
        </div>
      </div>
  </div>
    `)
  }

  // scrollIntoView(false) 代表滚动到最底部
  scroll()
})


// 发送图片
$('#file').on("change", function () {
  var file = this.files[0]

  //需要把这个文件发送到服务器，借助于H5新增的fileReader
  var fr = new FileReader()
  fr.readAsDataURL(file)
  fr.onload = function () {
    socket.emit("sendImage", {
      username: username,
      avatar: avatar,
      img: fr.result
    })
  }
})

// 监听聊天图片消息
socket.on("receiveImage", data => {
  // 把接收到的消息显示到聊天框中
  if (data.username === username) {
    // 自己的消息
    $('.box-bd').append(`
    <div class="message-box">
      <div class="my message">
        <img src="${data.avatar}" alt="" class="avatar">
        <div class="content">
        <div class="bubble">
          <div class="bubble_cont"><img scr="${data.img}"></div>
        </div>
      </div>
      </div>
  </div>
    `)
  } else {
    // 别人的消息
    $('.box-bd').append(`
    <div class="message-box">
      <div class="other message">
        <img src="${data.avatar}" alt="" class="avatar">
        <div class="nickname">${data.username}</div>
        <div class="content">
          <div class="bubble">
            <div class="bubble_cont"><img src="${data.img}"></div>
          </div>
        </div>
      </div>
  </div>
    `)
  }

  // 等待图片加载完成
  $('.box-bd img:last').on("load", function () {
    scroll()
  })

})

function scroll() {
  // scrollIntoView(false) 代表滚动到最底部
  $('.box-bd').children(':last').get(0).scrollIntoView(false)
}

// 初始化jquery-emoji插件 https://eshengsky.github.io/jQuery-emoji/
$('.face').on('click', function () {
  $('#content').emoji({
    button: '.face',
    showTab: false,
    animation: 'slide',
    position: 'topRight',
    icons: [{
      name: 'QQ表情',
      path: 'lib/jquery-emoji/img/emoji/',
      maxNum: 84,
      excludeNums: [41, 45, 54],
      file: '.png'
    }]
  })
})
