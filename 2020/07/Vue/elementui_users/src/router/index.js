import Vue from 'vue'
import VueRouter from 'vue-router'

import Index from "../views/Index"
import User from "../views/User"
import Order from "../views/Order"
import Msg from "../views/Msg"

Vue.use(VueRouter)

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

const routes = [
    {
        path: "/",
        redirect: "/index"
    },
    {
        path: "/index",
        name: "index",
        component: Index
    },
    {
        path: "/users",
        name: "users",
        component: User
    },
    {
        path: "/msgs",
        name: "msgs",
        component: Msg
    },
    {
        path: "/orders",
        name: "orders",
        component: Order
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
