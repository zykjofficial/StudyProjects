import axios from 'axios';

export function request(config) {
  const instance = axios.create({
    baseURL: "http://localhost:3000/",
    timeout: 5000
  })

  instance.interceptors.request.use(config => {
    // console.log("拦截的请求");
    // console.log(config)
    // 拦截了请求要返回
    return config;
  }, err => {
    console.log(err)
  })

  instance.interceptors.response.use(response => {
    // console.log("拦截的响应");
    // console.log(response);
    return response.data
  }, err => {
    console.log(err)
  })

  return instance(config);
}
