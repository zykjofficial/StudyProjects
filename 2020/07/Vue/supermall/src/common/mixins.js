import { debounce } from "./utils";
import BackTop from "components/contents/backtop/BackTop";
export const imgListenerMixin = {
  data() {
    return {
      imageListener: null,
      newRefresh: null
    }
  },
  mounted() {
    this.newRefresh = debounce(this.$refs.scroll.refresh, 50)

    this.imageListener = () => {
      this.newRefresh()
    }
    //创建时监听
    this.$bus.$on('imageLoad', this.imageListener)
  }
}

export const backTopMixin = {
  data() {
    return {
      isShowBackTop: false,
    }
  },
  components: {
    BackTop
  },
  methods: {
    topClick() {
      this.$refs.scroll.scrollTo(0, 0, 1000)
    },
  }
}


export const tabControlMixin = {
	data: function () {
		return {
			currentType: "pop"
		}
	},
	methods: {
		tabClick(index) {
			switch (index) {
				case 0:
					this.currentType = "pop"
					break
				case 1:
					this.currentType = "new"
					break
				case 2:
					this.currentType = "sell"
					break
			}
		}
	}
}