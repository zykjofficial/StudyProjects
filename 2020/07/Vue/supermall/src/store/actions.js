import {
  ADD_TO_CART,
  ADD_COUNTER,
  CLEAR_CART_LIST
} from "./mutations-types";

export default {
  //注意: 更新操作需要在mutations中进行
  addCart(context, payload) {
    return new Promise(((resolve, reject) => {
      let oldProduct = context.state.cartList.find(item => item.iid == payload.iid);
      if (oldProduct) {
        context.commit(ADD_COUNTER, oldProduct)
        resolve("当前商品数量+1")
      } else {
        payload.count = 1
        context.commit(ADD_TO_CART, payload)
        resolve("添加了此商品")
      }
    }))
  },
  clearCart(content,payload){
    return new Promise((resolve,reject)=>{
        content.commit(CLEAR_CART_LIST,payload)
        resolve('购买成功')
    })
  }
}
