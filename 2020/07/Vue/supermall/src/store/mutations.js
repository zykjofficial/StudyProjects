import {
  ADD_TO_CART,
  ADD_COUNTER,
  CLEAR_CART_LIST
} from "./mutations-types";

export default {
  [ADD_COUNTER](state, payload) {
    payload.count += 1
  },
  [ADD_TO_CART](state, payload) {
    payload.checked = true
    state.cartList.push(payload)
  },
  [CLEAR_CART_LIST](state, payload) {
      //通过将未选中的商品赋值给保存的数组、这样就达到了删除的效果
      state.cartList = payload
  }
}
