package com.zykj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElementUsersApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElementUsersApplication.class, args);
    }

}
