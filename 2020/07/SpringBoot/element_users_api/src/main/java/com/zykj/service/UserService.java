package com.zykj.service;

import com.zykj.entity.User;

import java.util.List;

public interface UserService {

    //查询所有
    public List<User> findAll();

    //保存用户
    public void save(User user);

    //删除用户
    public void delete(String id);

    //更新用户
    public void update(User user);

    //分页查询
    public List<User> findByPage(Integer pageNow, Integer rows);

    //查询总数
    public Long findTotals();
}
