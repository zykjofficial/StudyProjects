package com.zykj.vo;

import lombok.Data;

@Data
public class Result {
    //默认状态
    private boolean status = true;
    //返回信息
    private String msg;
}
