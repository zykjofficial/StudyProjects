package com.zykj.controller;

import com.alibaba.druid.util.StringUtils;
import com.zykj.entity.User;
import com.zykj.service.UserService;
import com.zykj.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("user")
//解决跨域问题
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("findall")
    public List<User> findAll() {
        return userService.findAll();
    }

    @PostMapping("saveOrUpdate")
    public Result save(@RequestBody User user) {
        Result reslut = new Result();
        try {
            if (StringUtils.isEmpty(user.getId())) {
                userService.save(user);
                reslut.setMsg("用户信息添加成功");
            } else {
                userService.update(user);
                reslut.setMsg("用户信息更新成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            reslut.setStatus(false);
            reslut.setMsg("发生错误、请稍后再试...");
        }
        return reslut;
    }

    @GetMapping("delete")
    public Result deleteOrUpdate(String id) {
        Result reslut = new Result();
        try {
            userService.delete(id);
            reslut.setMsg("用户信息删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            reslut.setStatus(false);
            reslut.setMsg("用户信息删除失败、请稍后再试...");
        }
        return reslut;
    }

    @GetMapping("findByPage")
    public Map<String, Object> findByPage(@RequestParam(value = "pageNow",defaultValue = "1") Integer pageNow,@RequestParam(value = "pageSize",defaultValue = "5") Integer pageSize) {
        Map<String, Object> result = new HashMap<>();
        List<User> users = userService.findByPage(pageNow, pageSize);
        Long totals = userService.findTotals();
        result.put("users", users);
        result.put("total", totals);
        return result;
    }
}
