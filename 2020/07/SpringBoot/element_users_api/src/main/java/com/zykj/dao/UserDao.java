package com.zykj.dao;

import com.zykj.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserDao {

    //查询所有
    public List<User> findAll();

    //保存用户
    public void save(User user);

    //更新用户
    public void update(User user);

    //删除用户
    public void delete(String id);

    //分页查询
    public List<User> findByPage(@Param("start") Integer start,@Param("rows") Integer rows);

    //查询总数
    public Long findTotals();

}
