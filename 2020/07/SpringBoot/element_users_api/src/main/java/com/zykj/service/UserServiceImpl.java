package com.zykj.service;

import com.zykj.dao.UserDao;
import com.zykj.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    // 支持当前事务，如果当前没有事务，就以非事务方式执行。
    @Transactional(propagation = Propagation.SUPPORTS)
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public void delete(String id) {
        userDao.delete(id);
    }

    @Override
    public List<User> findByPage(Integer pageNow, Integer rows) {
        int start = (pageNow - 1) * rows;
        return userDao.findByPage(start, rows);
    }

    @Override
    public Long findTotals() {
        return userDao.findTotals();
    }

}
