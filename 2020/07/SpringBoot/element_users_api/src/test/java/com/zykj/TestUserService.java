package com.zykj;


import com.zykj.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestUserService {

    @Autowired
    private UserService userService;

    @Test
    public void TestUserService() {
        userService.findAll().forEach(user -> System.out.println("User: " + user));
    }

}
